import React, { Component } from 'react'

class Box extends Component {
    constructor(props){
        super(props)
        this.handleRemove = this.handleRemove.bind(this)
    }
    handleRemove(){
        this.props.remove(this.props.id)
    }
    static defaultProps = {
        color: 'yellow',
        width: '100px',
        height: '100px'
    }
    render() {
        let styleObj = {
            backgroundColor: this.props.color,
            width: this.props.width,
            height: this.props.height,
            margin: '10px'
        }
        return (<div>
            <div style={styleObj}></div>
            <button onClick={this.handleRemove}>Remove Button</button>
        </div>)
    }
}

export default Box;