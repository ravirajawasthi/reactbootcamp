import React, { Component } from 'react';

class SimpleForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }
  handleSubmit(event) {
    event.preventDefault();
    alert(`Submitted value : ${this.state.username}`);
    this.setState({ username: '' });
  }
  render() {
    return (
      <div className="SimpleForm">
        <form onSubmit={this.handleSubmit}>
          <label htmlFor="uname">Username : </label>
          <input
            type="text"
            value={this.state.username}
            onChange={this.handleChange}
            name="username"
            id="uname"
          />
          <button>Submit</button>
        </form>
      </div>
    );
  }
}

export default SimpleForm;
