import React, { Component } from 'react';
import Box from './Box.js';
import uuid from 'uuid';

class BoxMaker extends Component {
  constructor(props) {
    super(props);

    this.state = {
      height: '100px',
      width: '100px',
      color: 'white',
      boxes: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.addBox = this.addBox.bind(this);
    this.removeBox = this.removeBox.bind(this);
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.addBox({
      color: this.state.color,
      height: this.state.height,
      width: this.state.width
    });
  }

  addBox(box) {
    let id = uuid();
    let newBoxes = [
      ...this.state.boxes,
      <Box
        color={box.color}
        height={box.height}
        width={box.width}
        key={id}
        id={id}
        remove={this.removeBox}
      />
    ];
    this.setState({ boxes: newBoxes });
  }
  removeBox(key) {
    let newBoxes = this.state.boxes.filter(box => {
      return box.props.id !== key;
    });
    this.setState({
      boxes: newBoxes
    });
  }

  render() {
    return (
      <div>
        <h1>BoxMaker Exercise</h1>
        <div>
          <form onSubmit={this.handleSubmit}>
            <label htmlFor="color">Color</label>
            <input
              type="text"
              id="color"
              name="color"
              onChange={this.handleChange}
            />

            <label htmlFor="width">Width</label>
            <input
              type="text"
              id="width"
              name="width"
              onChange={this.handleChange}
            />

            <label htmlFor="height">Height</label>
            <input
              type="text"
              id="height"
              name="height"
              onChange={this.handleChange}
            />

            <button>Submit!</button>
          </form>
        </div>
        {this.state.boxes}
      </div>
    );
  }
}

export default BoxMaker;
