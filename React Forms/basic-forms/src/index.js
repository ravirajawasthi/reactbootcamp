import React from 'react';
import ReactDOM from 'react-dom';
import SimpleForm from './SimpleForm';
import MultipleForms from './MultipleForms';
import ShoppingList from './ShoppingList';
import BoxMaker from './BoxMaker'

import './styles.css';

function App() {
  return (
    <div className="App">
      <SimpleForm />
      <h1>Form with multiple inputs</h1>
      <MultipleForms />
      <h1>
        Simple Shopping list to demostrate passing data from form to parent
        component
      </h1>
      <ShoppingList />
      <h1>Box Maker exercise</h1>
      <BoxMaker />
    </div>
  );
}

const rootElement = document.getElementById('root');
ReactDOM.render(<App />, rootElement);
