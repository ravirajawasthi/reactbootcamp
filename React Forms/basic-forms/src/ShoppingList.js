import React, { Component } from 'react';
import uuid from 'uuid';
import './ShoppingList.css';
import AddItem from './AddItem';

class ShoppingList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        { name: 'Joker Movie tickets', quantity: 2 },
        { name: 'Bosch Washing machine', quantity: 1 }
      ]
    };
    this.addItem = this.addItem.bind(this);
  }

  addItem(item) {
    let newItems = [...this.state.items, item];
    this.setState({ items: newItems });
  }

  render() {
    return (
      <div>
        <ul className="ShoppingList">
          {this.state.items.map(item => (
            <li key={uuid()}>
              {item.name} : {item.quantity}
            </li>
          ))}
        </ul>
        <AddItem addItem={this.addItem} />
      </div>
    );
  }
}

export default ShoppingList;
