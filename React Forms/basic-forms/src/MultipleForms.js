import React, { Component } from 'react';
class MultipleForms extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      name: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }
  handleSubmit(event) {
    event.preventDefault();
    alert(
      `Name : ${this.state.name}, email: ${this.state.email}, password: ${this.state.password}`
    );
    this.setState({
      name: '',
      email: '',
      password: ''
    });
  }
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label htmlFor="username">Name</label>
          <input
            type="text"
            value={this.state.name}
            id="username"
            name="name"
            onChange={this.handleChange}
          />
          <label htmlFor="email">Email</label>
          <input
            type="email"
            value={this.state.email}
            id="email"
            name="email"
            onChange={this.handleChange}
          />
          <label htmlFor="password">Password</label>
          <input
            type="password"
            value={this.state.password}
            id="password"
            name="password"
            onChange={this.handleChange}
          />
          <button>Submit!</button>
        </form>
      </div>
    );
  }
}

export default MultipleForms;
