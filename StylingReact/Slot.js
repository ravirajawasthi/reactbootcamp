class Slot extends React.Component{
    render(){
        let slots = [ "👇" ,"☝", "✋", "🤚"]
        let a = slots[Math.floor(Math.random() * 3)]
        let b = slots[Math.floor(Math.random() * 3)]
        let c = slots[Math.floor(Math.random() * 3)]
        console.log(slots);
        console.log(a, b, c);
        let result = a === b && a === c;
        let win = result ? "winner" : "loser";
        let h1Style = {fontSize: "50px", color: "purple" }
        return (
            <div>
                <h1 style = {h1Style}>{a}  {b}  {c}</h1>
                {result ? <h2  className = {win}>You Won</h2> : <h2  className = {win}>You Lose</h2>}
            </div>
        )
    }
}