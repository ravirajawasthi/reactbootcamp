class App extends React.Component{
    render(){
        return(
            <div>
                <h1>Welcome to my slot machine</h1>
                <Slot />
                <Slot />
                <Slot />
                <Slot />
                <Slot />
                <Slot />
                <Slot />
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById("root"))