import React, { Component } from 'react'
import Ball from './Ball'
import './Lottery.css'

class Lottery extends Component {
    static defaultProps = {
        title: "Lottery never works!",
        maxBalls: 6,
        maxNum: 50
    }
    constructor(props) {
        super(props)
        this.state = {
            nums: [...Array(this.props.maxBalls)].map(() => null)
        }
        this.handleClick = this.handleClick.bind(this)
        this.generateClick = this.generateClick.bind(this)
    }

    generateClick() {
        return this.state.nums.map(() => Math.floor(Math.random() * this.props.maxNum) + 1)
    }

    handleClick() {
        const newNums = this.generateClick()
        this.setState({ nums: newNums })
    }

    render() {
        return (
            <div className="Lottery">
                <h1>{this.props.title}</h1>
                {this.state.nums.map((n) => <Ball num={n} />)}
                <button onClick={this.handleClick}>Generate!</button>
            </div>
        )
    }
}

export default Lottery