import React, { useContext } from "react";
import Paper from "@material-ui/core/Paper";
import { withStyles } from "@material-ui/core/styles";
import { LanguageContext } from "./contexts/LanguageContext";
import LockOutlinesIcon from "@material-ui/icons/LockOpenOutlined";
import styles from "./styles/LoginFormStyles";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import FormHelperText from "@material-ui/core/FormHelperText";
import { Avatar, TextField, Button } from "@material-ui/core";

const words = {
  english: {
    email: "Email",
    password: "password",
    login: "Login!",
    formLabel: "Your password is encrypted. No one knows it, except you!"
  },
  french: {
    email: "Email",
    password: "mot de passe!",
    login: "S'identifier!",
    formLabel: "Votre mot de passe est crypté. Personne ne le sait, sauf vous!"
  },
  hindi: {
    email: "ईमेल",
    password: "पासवर्ड",
    login: "लॉग इन करें!",
    formLabel:
      "आपका पासवर्ड एन्क्रिप्ट किया गया है। इसे कोई नहीं जानता, तुम्हारे सिवा!"
  }
};

function LoginForm(props) {
  const { language, changeLanguage } = useContext(LanguageContext)
    const { classes } = props;
    return (
      <Paper elevation={3} className={classes.root}>
        <Avatar className={classes.avatar} alt="Default Avatar Image">
          <LockOutlinesIcon />
        </Avatar>
        <div className={classes.formControl}>
          <Select onChange={changeLanguage} value={language}>
            <MenuItem value={"english"}>English</MenuItem>
            <MenuItem value={"french"}>French</MenuItem>
            <MenuItem value={"hindi"}>Hindi</MenuItem>
          </Select>
        </div>
        <FormControl className={classes.form}>
          <TextField
            required
            id="email"
            aria-describedby="email-input"
            label={words[language]["email"]}
          />
          <TextField
            required
            id="password"
            label={words[language]["password"]}
            type="password"
            autoComplete="current-password"
          />
          <Button
            className={classes.submitButton}
            variant="contained"
            type="submit"
            color="primary"
            fullWidth
          >
            {words[language]["login"]}
          </Button>
          <FormHelperText id="my-helper-text">
            {words[language]["formLabel"]}
          </FormHelperText>
        </FormControl>
      </Paper>
    );
  
}
export default withStyles(styles, { withTheme: true })(LoginForm);
