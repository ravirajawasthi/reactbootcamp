import React from "react";
import Navbar from "./Navbar";
import LoginForm from "./LoginForm";
import PageContent from "./PageContent";
import { ThemeContextProvider } from "./contexts/ThemeContext";
import { LanguageContextProvider } from "./contexts/LanguageContext"
import "./App.css";

function App() {
  return (
    <ThemeContextProvider>
      <LanguageContextProvider>
      <PageContent className="App">
        <Navbar />
        <LoginForm />
      </PageContent>
      </LanguageContextProvider>
    </ThemeContextProvider>
  );
}

export default App;
