import React, { useContext } from "react";
import {ThemeContext} from "./contexts/ThemeContext"

function PageContent(props) {  
    const { children } = props;
    const {darkMode} = useContext(ThemeContext)
    const styles = {
        backgroundColor: darkMode ? "black":   "white",
        height: "100vh",
        widthL :"100vw"
    }
    return <div style={styles}>{children}</div>;
}

export default PageContent;
