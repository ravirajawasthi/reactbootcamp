import React, { useContext } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import Switch from "@material-ui/core/Switch";
import styles from "./styles/NavbarStyles";
import {LanguageContext} from "./contexts/LanguageContext"
import { ThemeContext } from "./contexts/ThemeContext";
import { withStyles } from "@material-ui/core/styles";

const strings = {
  english: {
    search: "Search",
    emoji: "🇬🇧"
  },
  french: {
    search: "Shercher",
    emoji: "🇫🇷"
  },
  hindi: {
    search: "खोजे",
    emoji: "🇮🇳"
  }
}

function Navbar(props) {

    const { classes } = props;
    const {toggleTheme, darkMode} = useContext(ThemeContext)
    const {language} = useContext(LanguageContext)
    return (
      <div className={classes.root}>
        <AppBar position="static" color={darkMode ? "default": "primary"}>
          <Toolbar>
            <IconButton color="inherit">
              <span role="img" aria-label="Selected Language flag">
                {strings[language]["emoji"]}
              </span>
            </IconButton>
            <Typography
              className={classes.title}
              variant="h6"
              noWrap
              color="inherit"
            >
              Context API
              <Switch onChange={toggleTheme}/>
            </Typography>

            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon color="inherit" />
              </div>
              <InputBase
                placeholder={strings[language]["search"]}
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput
                }}
              />
            </div>
          </Toolbar>
        </AppBar>
      </div>
    );
  
}

export default withStyles(styles, { withTheme: true })(Navbar);
