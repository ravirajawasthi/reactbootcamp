import {useState} from "react"

const useToggleState = (initialValue = false) => {
    const [value, setvalue] = useState(initialValue)
    const toggleValue = () => {
        setvalue(!value)
    }
    return [value, toggleValue]
}

export default useToggleState