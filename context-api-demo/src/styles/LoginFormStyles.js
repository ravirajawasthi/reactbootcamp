export default theme => ({
    "root": {
        width: "70%",
        margin: `${theme.spacing(3)}px auto`,
        padding: `${theme.spacing(2)}px 0`,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        [theme.breakpoints.up("sm")]: {
            width: "40%",
        },
        [theme.breakpoints.up("lg")]: {
            width: "20%",
        },
        
    },
    "avatar":{
        margin: "auto",
        backgroundColor: theme.palette.secondary.main,
    },
    "formControl":{
        marginTop: `${theme.spacing(1)}px`,
    },
    "submitButton": {
        marginTop: `${theme.spacing(2)}px`,
        marginLeft: "auto",
    },
    "form":{
        marginTop: `${theme.spacing(5)}px`,
        width: "70%"
    }
})