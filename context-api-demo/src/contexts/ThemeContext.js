import React, {  createContext } from "react";
import useToggleState from '../hooks/useToggleState'
const ThemeContext = createContext();

function ThemeContextProvider(props) {
  const [darkMode, toggleTheme] = useToggleState(false)

  
    return (
      <ThemeContext.Provider value={{darkMode, toggleTheme}}>
        {props.children}
      </ThemeContext.Provider>
    );
}

export {ThemeContextProvider, ThemeContext}