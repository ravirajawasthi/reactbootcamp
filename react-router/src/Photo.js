import React from 'react'

const Photo = function(props){
    const name = props.match.params.name;
    const reqUrl = `https://source.unsplash.com/800x450/?`+name;
    console.log(reqUrl)
    return(
            <div>
                <h1>Here's what we found</h1>
                <img src={reqUrl} alt={name} />
            </div>
    )
}

export default Photo;