import React, { Component } from 'react'
import {NavLink} from 'react-router-dom';
import vendingMacineImage from './vendingMachine.png';
import './VendingMachine.css';

class VendingMachine extends Component{
    render(){
        return(
            <div className="VendingMachine">
                <h1 className="VendingMachine-title">Hi i am vending Machine</h1>
                <div className="VendingMachine-machine">
                    <div className="VendingMachine-description">Everything is free</div>
                    <img src={vendingMacineImage} alt="Vending Machine" />
                    <ul>
                        <NavLink activeClassName="VendingMachine-active" className="VendingMachine-nav" to="/soda">Soda</NavLink>
                        <NavLink className="VendingMachine-nav" to="/salmon">Salmon</NavLink>
                        <NavLink className="VendingMachine-nav" to="/chips">Chips</NavLink>
                    </ul>
                </div>
            </div>
        )
    }
}

export default VendingMachine;