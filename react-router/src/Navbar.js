import React from 'react';
import {NavLink} from 'react-router-dom';
import './Navbar.css'

function Navbar(props){
    return(
        <ul className="Navbar">
            <NavLink exact activeClassName="Navbar-active" className="Navbar-nav" to="/soda">Soda</NavLink>
            <NavLink exact activeClassName="Navbar-active" className="Navbar-nav" to="/salmon">Salmon</NavLink>
            <NavLink exact activeClassName="Navbar-active" className="Navbar-nav" to="/chips">Chips</NavLink>
        </ul>
    )
}


export default Navbar;