import React from 'react';
import './App.css';
import VendingMachine from './VendingMachine';
import {Route, Switch} from 'react-router-dom';
import Chips from './Chips'
import Soda from './Soda'
import Salmon from './Salmon'
import Notfound from './Notfound'
import PhotoSearch from './PhotoSearch'
import Photo from './Photo'


function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path='/' component={VendingMachine} />

        <Route exact path='/salmon' component={Salmon} />

        <Route exact path='/chips' component={Chips} />

        <Route exact path='/soda' component={Soda} />

        <Route exact path='/photo' component={PhotoSearch} />

        <Route exact path='/:name' component={Notfound} />

        <Route exact path='/photo/:name' component={Photo} />
      </Switch>

    </div>
  );
}

export default App;
