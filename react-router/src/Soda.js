import React from 'react'
import {NavLink} from 'react-router-dom'
import Navbar from './Navbar'

function Soda(props) {
    return <div>
            <Navbar />
            <h1>COLA</h1>
            <NavLink to='/'>Go Back!</NavLink>
        </div>;
}

export default Soda