import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class PhotoSearch extends Component{
    constructor(props){
        super(props)
        this.state={
            query: ""
        }
        this.handleChange = this.handleChange.bind(this)
    }
    

    handleChange(evt){
        this.setState({query: evt.target.value});
    }

    render(){
        return(
            <div>
                <h1>Search for a image</h1>
                <input type="text" name="query" onChange={this.handleChange}/>
                <Link to={`/photo/${this.state.query}`}>Search</Link>
            </div>
        )
    }
    
}

export default PhotoSearch;