import React from 'react'
import {NavLink} from 'react-router-dom';
import Navbar from './Navbar';

function Salmon(props) {
    return <div>
            <Navbar />
            <h1>YUP! Salmon</h1>
            <NavLink to='/'>Go Back!</NavLink>
        </div>;
}

export default Salmon