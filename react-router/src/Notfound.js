import React from 'react'

const Notfound = function(props){
    const name = props.match.params.name;
    return (
        <h1>We dont have {name} right now!</h1>
    )
}

export default Notfound;
