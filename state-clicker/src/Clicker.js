import React, { Component } from 'react'

class Clicker extends Component{
    constructor(props){
        super(props);
        this.state = {
            "win" : false,
            "currentNumber": 0
        }
        this.generateRandomNumber = this.generateRandomNumber.bind(this);
    }
    generateRandomNumber(e){
        let num = Math.floor(Math.random() * 10) + 1;
        this.setState({"currentNumber": num});
        if (num === 7){
            this.setState({win : true});
            console.log("You won!")
        }
    }

    render(){
        return(
            <div>
                <h1>You got : {this.state.currentNumber}</h1>
                {this.state.win?<h1>You Won </h1>: <button onClick={this.generateRandomNumber}>Click Me!</button>}
            </div>
        )
    }
}

export default Clicker