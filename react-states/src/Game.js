import React, { Component } from 'react'

class Game extends Component {
    constructor(props) {
        super(props)
        this.state = {
            "score": 0,
            "gameOver": false
        }
    };
    
    incrementScore() {
        setInterval(() => {
            this.setState({ score: this.state.score + 1 });
            console.log("Score is incremented to : " + this.state.score);
        }, 1000)
    };
    
    render() {
        return (
            <div>
                <h1>From inside game Component</h1>
                <h1>Your current score is : {this.state.score}</h1>
            </div>

        )
    }
}


export default Game;