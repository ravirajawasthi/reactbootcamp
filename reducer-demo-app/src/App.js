import React , {useReducer} from 'react';
import './App.css';

const reducer = (state, action) => {
  if(action.type==="INCREMENT"){
    return ({count: state.count + action.amount})
  }
  else if(action.type==="DECREMENT"){
    return ({count: state.count - action.amount})
  }
  else if(action.type==="RESET"){
    return ({count: 0})
  }
}

function App() {
  const [state, dispatch] = useReducer(reducer, {count: 0})
  return (
    <div className="App">
      <h1>Count : {state.count}</h1>
      <button onClick={() => dispatch({type: "INCREMENT", amount: 1})}>ADD 1</button>
      <button onClick={() => dispatch({type: "INCREMENT", amount: 5})}>ADD 5</button>
      <button onClick={() => dispatch({type: "DECREMENT", amount: 1})}>SUBTRACT 1</button>
      <button onClick={() => dispatch({type: "RESET"})}>RESET</button>
    </div>
  );
}

export default App;
