import React, { Component } from 'react';
import Dice from './Dice';
import './RollDice.css';

class RollDice extends Component {
  static defaultProps = {
    sides: ['one', 'two', 'three', 'four', 'five', 'six'],
    noOfDice: 6
  };
  constructor(props) {
    super(props);
    this.state = {
      dices: [
        <Dice face="one" key={Math.random()} rolling={false} />,
        <Dice face="one" key={Math.random()} rolling={false} />,
        <Dice face="one" key={Math.random()} rolling={false} />,
        <Dice face="one" key={Math.random()} rolling={false} />,
        <Dice face="one" key={Math.random()} rolling={false} />,
        <Dice face="one" key={Math.random()} rolling={false} />
      ],
      rolling: false
    };
    this.roll = this.roll.bind(this);
    this.getNewFaces = this.getNewFaces.bind(this);
  }
  getNewFaces() {
    const dices = [];
    for (let i = 0; i < this.props.noOfDice; i += 1) {
      let n = Math.floor(Math.random() * 6) + 1;
      console.log(this.state.rolling);
      dices.push(
        <Dice
          face={this.props.sides[n - 1]}
          rolling={this.state.rolling}
          key={Math.random()}
        />
      );
    }
    this.setState({ dices: dices });

    //wait one second, then set rolling to false
    setTimeout(() => {
      this.setState({ rolling: false });
    }, 1000);
  }
  roll() {
    this.setState({ rolling: true }, this.getNewFaces);
  }
  render() {
    return (
      <div className="RollDice">
        <div className="RollDice-container">{this.state.dices}</div>
        <button onClick={this.roll} disabled={this.state.rolling}>
          {this.state.rolling ? 'Rolling...' : 'Roll Dice!'}
        </button>
      </div>
    );
  }
}

export default RollDice;
