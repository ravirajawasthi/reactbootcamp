import React from 'react';
import Palette from './Palette'
import './App.css';

function App() {
  return (
    <div className="App">
      <Palette />
    </div>
  );
}

export default App;
