import React, { Component } from 'react'
import './Color.css'

class Color extends Component {
    constructor(props) {
        super(props)
        this.state = {
            color: this.generateColor()
        }
        this.generateColor = this.generateColor.bind(this)
        this.handleClick = this.handleClick.bind(this)
    }

    generateColor() {
        return '#' + Math.floor(Math.random() * 16777215).toString(16);
    }

    handleClick() {
        const newColor = this.generateColor();
        this.setState({ color: newColor });
    }

    render() {
        const divStyle = {
            backgroundColor: this.state.color
        }
        return (
            <div className="Color" style={divStyle} onClick={this.handleClick}></div>
        )
    }

}

export default Color;