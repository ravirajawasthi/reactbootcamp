import React, { Component } from 'react'
import Color from './Color.js'
import './Palette.css'

class Palette extends Component {
    static defaultProps = {
        maxColors: 12
    }
    render() {
        const palette = []

        for (let i = 0; i < this.props.maxColors; i += 1) {
            palette.push(<Color key={Math.random()} />)
        }

        return (
            <div className="Palette">
                {palette}
            </div>
        )
    }
}

export default Palette;