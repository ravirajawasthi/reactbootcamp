class NumPicker extends React.Component {
    render() {
      let num = Math.floor(Math.random() * 10) + 1;
      const winningMessage = (
        <div>
          <h1>You Won dude</h1>
          <img src="https://media.giphy.com/media/QXPmPdudTz4So2P4OQ/giphy.gif" />
        </div>
      );
      const loosingMessage = <h1>You are so unlucky</h1>;
      return (
        <div>
          <h1>You got {num}</h1>
          {num !== 7 && loosingMessage}
          {num === 7 && winningMessage}
        </div>
      );
    }
  }
  