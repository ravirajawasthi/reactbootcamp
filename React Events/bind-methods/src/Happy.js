import React, { Component } from 'react';
import './Happy.css';
class Happy extends Component {
  static defaultProps = {
    messages: ['Quote 1', 'Quote 2', 'Quote 3']
  };
  constructor(props) {
    super(props);
    this.state = {
      happy: false,
      quote: this.props.messages[
        Math.floor(Math.random() * this.props.messages.length)
      ]
    };
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
  }

  handleMouseOver(evt) {
    console.log(this.despenseQuote());
    this.setState({ happy: true });
  }
  handleMouseLeave(evt) {
    this.setState({ happy: false });
  }
  despenseQuote() {
    return this.props.messages[
      Math.floor(Math.random() * this.props.messages.length)
    ];
  }
  render() {
    return (
      <div
        onMouseLeave={this.handleMouseLeave}
        onMouseOver={() => {
          this.handleMouseOver();
        }}
        className={this.state.happy ? 'Happy Happy-happy' : 'Happy Happy-sad'}
      >
        <div className="Happy-emoji">{this.state.happy ? '😊' : '😢'}</div>
      </div>
    );
  }
}

export default Happy;
