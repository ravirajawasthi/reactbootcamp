import React from 'react';
import './App.css';
import Happy from './Happy.js';
import Color from './Color.js';
import Todo from './Todo.js';

function App() {
  return (
    <div className="App">
      <Happy />
      <Color />
      <Todo />
    </div>
  );
}

export default App;
