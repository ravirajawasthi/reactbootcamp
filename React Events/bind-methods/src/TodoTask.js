import React, { Component } from 'react';
import './TodoTask.css';

class TodoTask extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(evt) {
    this.props.remove(this.props.task);
  }
  render() {
    return (
      <ul className="TodoTask">
        <li>
          {this.props.task}
          <button onClick={this.handleClick}>X</button>
        </li>
      </ul>
    );
  }
}

export default TodoTask;
