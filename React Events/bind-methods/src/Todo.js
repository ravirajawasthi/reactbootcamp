import React, { Component } from 'react';
import TodoTask from './TodoTask.js';

class Todo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [1, 2, 3, 4, 5, 6, 7, 8, 8]
    };
    this.remove = this.remove.bind(this);
  }
  remove(num) {
    this.setState({
      tasks: this.state.tasks.filter(n => n !== num)
    });
  }
  render() {
    return (
      <div>
        {this.state.tasks.map(task => {
          return <TodoTask task={task} remove={this.remove} />;
        })}
      </div>
    );
  }
}

export default Todo;
