import React, { Component } from 'react';
import './Color.css';

class Color extends Component {
  static defaultProps = {
    colors: ['#6b5b95', '#feb236', '#d64161', '#ff7b25']
  };
  constructor(props) {
    super(props);
    this.state = {
      bgColor: {
        backgroundColor: 'white'
      }
    };
  }
  handleClick(c) {
    this.setState(() => {
      const newStyleObj = {
        backgroundColor: c
      };
      return { bgColor: newStyleObj };
    });
  }
  render() {
    return (
      <div className="Color" style={this.state.bgColor}>
        {this.props.colors.map(c => {
          const styleObj = { backgroundColor: c };
          return (
            <button
              style={styleObj}
              onClick={this.handleClick.bind(this, c)}
              key={Math.random()}
            >
              Click me!
            </button>
          );
        })}
      </div>
    );
  }
}

export default Color;
