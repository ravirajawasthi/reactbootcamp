import React from 'react';
import './App.css';
import Happy from './Happy';
import AnnoyingInput from './AnnoyingInput';
import ClipboardEvent from './ClipboardEvent';

function App() {
  return (
    <div className="App">
      <Happy />
      <AnnoyingInput />
      <ClipboardEvent />
    </div>
  );
}

export default App;
