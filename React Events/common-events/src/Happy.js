import React, { Component } from 'react';
import './Happy.css';
class Happy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      happy: false
    };
    this.handleMouseOver = this.handleMouseOver.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);
  }

  handleMouseOver(evt) {
    this.setState({ happy: true });
  }
  handleMouseLeave(evt) {
    this.setState({ happy: false });
  }

  render() {
    return (
      <div
        onMouseLeave={this.handleMouseLeave}
        onMouseOver={this.handleMouseOver}
        className={this.state.happy ? 'Happy Happy-happy' : 'Happy Happy-sad'}
      >
        <div className="Happy-emoji">{this.state.happy ? '😊' : '😢'}</div>
      </div>
    );
  }
}

export default Happy;
