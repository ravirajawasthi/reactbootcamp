import React, { Component } from 'react';

class AnnoyingInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: ''
    };
    this.handleKeyUp = this.handleKeyUp.bind(this);
  }
  handleKeyUp(e) {
    const keyPressed = e.key;
    this.setState(st => {
      return {
        input: this.state.input + keyPressed
      };
    });
  }
  render() {
    return (
      <div>
        <input
          type="text"
          onKeyUp={this.handleKeyUp}
          placeholder="I only like *"
          value={this.state.input}
        />
        <h1>{this.state.input}</h1>
      </div>
    );
  }
}

export default AnnoyingInput;
