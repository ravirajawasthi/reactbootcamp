import React, { Component } from 'react'
import './Coin.css'

class Coin extends Component {
    static defaultProps = {
        faces: null
    }

    render() {
        const img = this.props.face ? <img className="Coin" alt="Coin Face" src={this.props.face} /> : <h1></h1>
        return (
            img
        )

    }
}
export default Coin;