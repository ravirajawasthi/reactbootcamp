import React, { Component } from 'react'
import './CoinTosser.css'
import Coin from './Coin.js'

class CoinTosser extends Component {
    static defaultProps = {
        head: "https://upload.wikimedia.org/wikipedia/commons/c/cd/S_Half_Dollar_Obverse_2016.jpg",
        tail: "https://upload.wikimedia.org/wikipedia/en/c/c0/Canadian_Dime_-_obverse.png",
        faces: ["head", "tail"]
    }
    constructor(props) {
        super(props)
        this.state = {
            heads: 0,
            tails: 0,
            allTosses: 0,
            face: null
        }
        this.incrementResult = this.incrementResult.bind(this)
        this.handleClick = this.handleClick.bind(this)

    }

    incrementResult(result) {
        if (result === "head") {
            this.setState(currState => ({ heads: currState.heads + 1 }));
        }
        else if (result === "tail") {
            this.setState(currState => ({ tails: currState.tails + 1 }));
        }
        this.setState(currState => ({ allTosses: currState.allTosses + 1 }));
        this.setState(currState => ({ face: result }));
        console.log(this.props[this.state.face])
    }

    handleClick() {
        const outcome = this.props.faces[Math.floor(Math.random() * this.props.faces.length)];
        this.incrementResult(outcome);
    }

    render() {
        return (
            <div className="CoinTosser">
                <h1>Lets' Tossem' up!</h1>
                <Coin face={this.props[this.state.face]} />
                <h1>Out of {this.state.allTosses} tosses you got {this.state.heads} heads and {this.state.tails} tails</h1>
                <button onClick={this.handleClick}>Flip meee!</button>
            </div>
        )
    }
}

export default CoinTosser;