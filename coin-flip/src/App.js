import React from 'react';
import CoinTosser from "./CoinTosser.js"
import './App.css';

function App() {
  return (
    <div className="App">
      <CoinTosser />
    </div>
  );
}

export default App;
