import React from 'react';
import './App.css';
import Timer from './Timer'
import ZenQuote from './ZenQuote'
import GithubUserInfo from './githubUserInfo'

function App() {
  return (
    <div className="App">
      <Timer />
      <ZenQuote />
      <GithubUserInfo username = "ravirajawasthi" />
      <GithubUserInfo username = "acdlite" />
    </div>
  );
}

export default App;
