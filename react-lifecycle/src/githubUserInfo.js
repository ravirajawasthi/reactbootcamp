import React, { Component } from 'react'
import Axios from "axios";

class GithubUserInfo extends Component{
    constructor(props){
        super(props)
        this.state = {name: "", avatarUrl: ""}
    }
    componentDidMount(){
        Axios.get(`https://api.github.com/users/${this.props.username}`).then(respone => {
            this.setState({
                name: respone.data.name,
                avatarUrl: respone.data.avatar_url
            })
        })
    }
    render(){
        
        return(
            <div>
                <h1>Name : {this.state.name}</h1>
                <img height = "150px" src = {this.state.avatarUrl} alt = {"Avatar"}/>
            </div>
        )
    }
}

export default GithubUserInfo