import React, { Component } from 'react'
import Axios from 'axios'
import './ZenQuote.css'

class ZenQuote extends Component{
    constructor(props){
        super(props)
        this.state  = {quote: "", isLoaded: false}
    }
    componentDidMount(){
        Axios.get("https://api.github.com/zen").then((response) => {
            setTimeout(() => {this.setState({quote : response.data, isLoaded: true})},500)  
        })
    }
    render(){
        return(
            
                this.state.isLoaded ?
                    <div>
                        <h1>Today's quote is : </h1>
                        <p>{this.state.quote}</p>
                    </div>
                :
                    <div className='loader'>
                        <div className='lt'></div>
                        <div className='rt'></div>
                        <div className='lb'></div>
                        <div className='rb'></div>
                    </div>
            
        )
    }
}

export default ZenQuote