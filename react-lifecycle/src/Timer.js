import React, { Component } from 'react'

class Timer extends Component{
    constructor(props){
        super(props)
        this.state = {timer: 0}
    }
    componentDidMount(){
        setInterval(() => this.setState(st => {
            console.log("updating timer")
            return {timer: st.timer + 1}
        }),1000)
    }
    render(){
        return(
            <h1>{this.state.timer}</h1>
        )
    }
}

export default Timer;