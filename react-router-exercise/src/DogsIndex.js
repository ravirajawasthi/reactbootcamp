import React from 'react'
import DogsInfo from './DogsInfo';
import { NavLink, Redirect } from 'react-router-dom';

const DogsIndex = function (props) {
    const name = props.match.params.name
    let foundDog = undefined
    DogsInfo.dogs.map((dog) => {
        if (dog.name.toLowerCase() === name) {
            foundDog = dog;
        }
    })
    if (foundDog) {
        return (
            <div>
                <h1>{foundDog.name}</h1>
                <img src={foundDog.src} alt={foundDog.name} />
                <ul>
                    {foundDog.facts.map(fact => <li>{fact}</li>)}
                </ul>
                <NavLink to='/'>Go Back</NavLink>
            </div>
        )
    } else {
        return (
            <Redirect to="/" />
        )
    }
}

export default DogsIndex;