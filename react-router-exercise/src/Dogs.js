import React from 'react';
import DogsInfo from './DogsInfo';
import { NavLink } from 'react-router-dom'


const Dogs = function (props) {
    return (
        <div>
            {
                DogsInfo.dogs.map(dog =>
                    <div>
                        <img src={dog.src} alt={dog.name} />
                        <NavLink className="display-3" to={`/dog/${dog.name.toLowerCase()}`}>{dog.name}</NavLink>
                    </div>
                )
            }
        </div>
    )
}



export default Dogs