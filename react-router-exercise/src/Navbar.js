import React from 'react'
import { NavLink } from 'react-router-dom';
import DogInfo from './DogsInfo';

const Navbar = function (props) {
    return (
        <nav className="navbar navbar-expand-md navbar-dark bg-dark">
            <NavLink to="/" className="navbar-brand">Dogs <span role="img" aria-label="heart">❤️</span></NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    {
                        DogInfo.dogs.map(dog => <li className="nav-item">
                            <NavLink activeClassName="active" to={`/dog/${dog.name.toLowerCase()}`} className="nav-link">{dog.name}</NavLink>
                        </li>
                        )
                    }
                </ul>
            </div>
        </nav>
    )
}


export default Navbar;