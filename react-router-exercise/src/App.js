import React from 'react';
import './App.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './Navbar';
import { Switch, Route } from 'react-router-dom';
import Dogs from './Dogs';
import DogsIndex from './DogsIndex';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Switch>
        <Route exact path='/' component={Dogs} />
        <Route exact path='/dog/:name' component={DogsIndex} />
      </Switch>
    </div>
  );
}


export default App;
