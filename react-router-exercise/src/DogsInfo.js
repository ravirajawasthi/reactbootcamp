import hazel from './images/hazel.jpg';
import tubby from './images/tubby.jpg';
import whiskey from './images/whiskey.jpg';
import range from './images/range.png';

const DogsInfo = {
    dogs: [
        {
            name: "Whiskey",
            age: 5,
            src: whiskey,
            facts: [
                "Whiskey loves eating popcorn.",
                "Whiskey is a terrible guard dog.",
                "Whiskey wants to cuddle with you!"
            ]
        },
        {
            name: "Hazel",
            age: 3,
            src: hazel,
            facts: [
                "Hazel has soooo much energy!",
                "Hazel is highly intelligent.",
                "Hazel loves people more than dogs."
            ]
        },
        {
            name: "Tubby",
            age: 4,
            src: tubby,
            facts: [
                "Tubby is not the brightest dog",
                "Tubby does not like walks or exercise.",
                "Tubby loves eating food."
            ]
        },
        {
            name: "Range",
            age: 8,
            src: range,
            facts: [
                "Range is the brightest dog",
                "Range loves all the walk he can get",
                "Range loves eating food."
            ]
        }
    ]
}

export default DogsInfo;