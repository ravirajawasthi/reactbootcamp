class Types extends React.Component{
    render(){
        return(
            <div>
                <h1>This parcel is for {this.props.for} from {this.props.from}</h1>
                <h3>This is array passed using props {this.props.list} </h3>
                <h3>This is a boolean value passed usig props {this.props.bool}</h3>
                <h3>This is number passed using props {this.props.num} {this.props.num2} </h3>
                <h3>The parcel is</h3>
                <img src = {this.props.src}/>
            </div>
        )
    }
}