import React from 'react';
import './App.css';
import Score from "./Score"
import ScoreImproved from "./ScoreImproved"


function App() {
  return (
    <div className="App">
      <Score />
      <h1>Using 3 state changes for for triple kill</h1>
      <h1>Only last one is applied because setState calls are bundled together</h1>
      <hr />
      <ScoreImproved />
    </div>
  )
}

export default App;
