import React, { Component } from "react";

class ScoreImproved extends Component {
    constructor(props) {
        super(props);
        this.state = {
            score: 0
        };
        this.singleKill = this.singleKill.bind(this);
        this.tripleKill = this.tripleKill.bind(this);
        this.incrementScore = this.incrementScore.bind(this)
    }
    incrementScore(currState) {
        return { score: currState.score += 1 }
    }
    singleKill() {
        this.setState(this.incrementScore);
    }
    tripleKill() {
        this.setState(this.incrementScore);
        this.setState(this.incrementScore);
        this.setState((currState) => ({ score: currState.score + 1 }));
    }
    render() {
        return (
            <div>
                <h1>Score : {this.state.score}</h1>
                <h1>setState has been given a function</h1>
                <button onClick={this.singleKill}>Single Kill</button>
                <button onClick={this.tripleKill}>Triple Kill</button>
            </div>
        );
    }
}

export default ScoreImproved;