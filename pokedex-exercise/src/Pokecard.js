import React, { Component } from "react";
import Pokemon from "./Pokemon";
import "./Pokecard.css"

class Pokecard extends Component{
    render(){
        console.log(this.props)
        return(
            <div className="Pokecard">
                <Pokemon 
                    name = {this.props.info.name}
                    type = {this.props.info.type}
                    xp = {this.props.info.base_experience}
                    id = {this.props.info.id}
                /> 
            </div>
        )
    }
}

export default Pokecard