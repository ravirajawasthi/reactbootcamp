import React, {Component} from "react";
import Pokecard from "./Pokecard";
import data from "./data";
import "./Pokegame.css"

class Pokegame extends Component {
    render() {
        let winningMessage = <h1 className = "Pokegame-win-message">You Won</h1>
        let losingMessage = <h1 className = "Pokegame-lose-message">You Lose</h1>
        let hand1 = {
            pokemons : [],
            score: 0,
            message: <h1>Message</h1>
        }
        let hand2 = {
            pokemons: [],
            score : 0,
            message: <h1>Message</h1>
        }
        let n = data.length/2
        let pokecards = []
        for (let i = 0; i < data.length; i++){
            if (i < n){
                hand1.pokemons.push(<Pokecard info = {data[i]} />)
                hand1.score += data[i].base_experience
            } else {
                hand2.pokemons.push(<Pokecard info = {data[i]} />)
                hand2.score += data[i].base_experience
            }
            if (hand1.score > hand2.score){
                hand1.message = winningMessage
                hand2.message = losingMessage
            } else {
                hand2.message = losingMessage
                hand1.message = winningMessage
            }
        }
        return (
            <div className = "Pokegame">
                {hand1.message}
                {hand1.pokemons}
                {hand2.message}
                {hand2.pokemons}
            </div>    
        )
    }
}


export default Pokegame;