import React, {Component} from "react";
import "./Pokemon.css"

class Pokemon extends Component{
    render(){
        let id = "0".repeat(3 - (this.props.id).toString().length) + this.props.id.toString();
        let name = this.props.name;
        let type = this.props.type;
        let xp = this.props.xp;
        let imageURL = `https://assets.pokemon.com/assets/cms2/img/pokedex/detail/${id}.png`
        console.log(imageURL)
        return(
            <div className = "Pokemon">
                <img className = "Pokemon-img" alt = "Pokemon" src = {imageURL} />
                <div className = "Pokemon-info">
                    <p><strong>{name}</strong></p>
                    <p><em>Type : {type}</em></p>
                    <p><em>XP : {xp}</em></p>
                </div>
            </div>
        )
    }
}


export default Pokemon
