import React from "react";
import useInputState from "./hooks/useInputState";

function InputHooks(props) {
  const [email, setEmail, resetEmail] = useInputState();
  const [password, setPassword, resetPassword] = useInputState();
  return (
    <div>
      <h1>
        Email is {email} && password is {password}
      </h1>
      <input
        type="text"
        value={email}
        onChange={setEmail}
        placeholder="Enter Email"
      />
      <input
        type="text"
        value={password}
        onChange={setPassword}
        placeholder="Enter Password"
      />
      <button onClick={resetEmail}>Clear Email</button>
      <button onClick={resetPassword}>Clear Password</button>
    </div>
  );
}

export default InputHooks;