import React from 'react';
import Counter from './Counter';
import CounteHooks from './CounterHooks';
import Toggler from './Toggler';
import InputHooks from './InputHooks';
import Clicker from './Clicker'
import MovieSW from './MoiveSW';
import './App.css';

function App() {
  return (
    <div className="App">
      <Counter />
      <CounteHooks />
      <Toggler />
      <InputHooks />
      <Clicker />
      <MovieSW />
    </div>
  );
}

export default App;
