import React from 'react';
import useToggle from './hooks/useToggle';

function Toggler(props){
    const [isHappy, toggleIsHappy] = useToggle(true);
    const [isApple, toggleIsApple] = useToggle(false);
    const [isGood, toggleIsGood] = useToggle(true);
    return(
        <div>
            <h1 onClick={toggleIsHappy}>{isHappy ? "😄":"😢" }</h1>
            <h1 onClick={toggleIsApple}>{isApple ? "🍎":"🍏" }</h1>
            <h1 onClick={toggleIsGood}>{isGood ? "👍":"👎" }</h1>
        </div>
    )

}

export default Toggler;