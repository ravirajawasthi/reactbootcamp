import {useState} from 'react';

function useInputState(){
    const [value, setstate] = useState("");
    const setValue = (e) => {
        setstate(e.target.value);
    }
    const reset = () => {
        setstate("");
    }

    return [value, setValue, reset];
}

export default useInputState