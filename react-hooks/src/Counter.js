import React, { Component } from 'react'

class Counter extends Component{
    constructor(props){
        super(props)
        this.state = {
            count: 0
        }
        this.incrementCounter = this.incrementCounter.bind(this);
    }
    incrementCounter(){
        this.setState(st => {
            return {count: st.count + 1}
        })
    }
    render(){
        return (
            <div>
                <h1>Normal Counter</h1>
                <h1>Count : {this.state.count}</h1>
                <button onClick={this.incrementCounter}>Add!</button>
            </div>
        )
    }
}

export default Counter;