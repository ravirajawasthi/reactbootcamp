import React , {useState} from 'react';

function CounterHooks(props){
    const [count, setCount] = useState(0);
    return(
        <div>
            <h1>Counter based on hooks</h1>
            <h1>Count : {count}</h1>
            <button onClick={() => setCount(count + 1)}>Add!</button>
        </div>
    )
}

export default CounterHooks;