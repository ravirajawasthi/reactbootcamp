import fruits from "./food"
import {choice, remove} from "./helpers"


let fruitChoice = choice(fruits);

console.log("Random selected fruit is " + fruitChoice);

var result  = remove(fruits, fruitChoice);

if (result === undefined){
    console.log(fruitChoice + " Is not in our list");
} else {
    console.log("We have " + fruitChoice + " in out list");
}

console.log("He have " + fruits.length + " in our basket")
