function choice(items) {
    return items[Math.floor(Math.random() * items.length) + 0]
}

function remove(items, item) {
    console.log("I will try to delete " + item + " from " + items)
    for (let i = 0; i < items.length; i++) {
        if (items[i] === item) {
            items.splice(i, 1)
            return true
        }
    }
    return false;
}


export {choice, remove}